package com.cocktailplease.anthony.cocktailplease.db.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cocktailplease.anthony.cocktailplease.R;
import com.cocktailplease.anthony.cocktailplease.db.entities.Cocktail;
import com.cocktailplease.anthony.cocktailplease.db.entities.CocktailIngredient;
import com.cocktailplease.anthony.cocktailplease.db.entities.Favorite;
import com.cocktailplease.anthony.cocktailplease.db.entities.Ingredient;
import com.cocktailplease.anthony.cocktailplease.db.entities.IngredientCategory;
import com.cocktailplease.anthony.cocktailplease.db.entities.Preference;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by anthony on 24/02/16.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "cocktailsplease.db";
    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 1;

    private Context context;

    // the DAO object we use to access the Ingredient table
    private Dao<Ingredient, Integer> ingredientDao = null;
    private RuntimeExceptionDao<Ingredient, Integer> ingredientRuntimeDao = null;

    // the DAO object we use to access the Cocktail table
    private Dao<Cocktail, Integer> cocktailDao = null;
    private RuntimeExceptionDao<Cocktail, Integer> cocktailRuntimeDao = null;

    // the DAO object we use to access the IngredientCategory table
    private Dao<IngredientCategory, Integer> ingredientCategoryDao = null;
    private RuntimeExceptionDao<IngredientCategory, Integer> ingredientCategoryRuntimeDao = null;

    // the DAO object we use to access the CocktailIngredient table
    private Dao<CocktailIngredient, Integer> cocktailIngredientDao = null;
    private RuntimeExceptionDao<CocktailIngredient, Integer> cocktailIngredientRuntimeDao = null;

    // the DAO object we use to access the Preference table
    private Dao<Preference, Integer> preferenceDao = null;
    private RuntimeExceptionDao<Preference, Integer> preferenceRuntimeDao = null;

    // the DAO object we use to access the Favorite table
    private Dao<Favorite, Integer> favoriteDao = null;
    private RuntimeExceptionDao<Favorite, Integer> favoriteRuntimeDao = null;




    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context.getApplicationContext();
    }


    /**
     * This is called when the database is first created. Usually you should call createTable statements here to create
     * the tables that will store your data.
     */
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, IngredientCategory.class);
            TableUtils.createTable(connectionSource, Ingredient.class);
            TableUtils.createTable(connectionSource, Cocktail.class);
            TableUtils.createTable(connectionSource, CocktailIngredient.class);
            TableUtils.createTable(connectionSource, Preference.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }
        createEntities();
    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, Ingredient.class, true);
            // after we drop the old databases, we create the new ones
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }



    /**
     * Returns the Database Access Object (DAO) for our Ingredient class. It will create it or just give the cached
     * value.
     */
    public Dao<Ingredient, Integer> getIngredientDao() throws SQLException {
        if (ingredientDao == null) {
            ingredientDao = getDao(Ingredient.class);
        }
        return ingredientDao;
    }



    /**
     * Returns the RuntimeExceptionDao (Database Access Object) version of a Dao for our Ingredient class. It will
     * create it or just give the cached value. RuntimeExceptionDao only through RuntimeExceptions.
     */
    public RuntimeExceptionDao<Ingredient, Integer> getIngredientRuntimeDao() {
        if (ingredientRuntimeDao == null) {
            ingredientRuntimeDao = getRuntimeExceptionDao(Ingredient.class);
        }
        return ingredientRuntimeDao;
    }


    /**
     * Returns the Database Access Object (DAO) for our Ingredient class. It will create it or just give the cached
     * value.
     */
    public Dao<Cocktail, Integer> getCocktailDao() throws SQLException {
        if (cocktailDao == null) {
            cocktailDao = getDao(Cocktail.class);
        }
        return cocktailDao;
    }


    /**
     * Returns the RuntimeExceptionDao (Database Access Object) version of a Dao for our Cocktail class. It will
     * create it or just give the cached value. RuntimeExceptionDao only through RuntimeExceptions.
     */
    public RuntimeExceptionDao<Cocktail, Integer> getCocktailRuntimeDao() {
        if (cocktailRuntimeDao == null) {
            cocktailRuntimeDao = getRuntimeExceptionDao(Cocktail.class);
        }
        return cocktailRuntimeDao;
    }


    /**
     * Returns the Database Access Object (DAO) for our IngredientCategory class. It will create it or just give the cached
     * value.
     */
    public Dao<IngredientCategory, Integer> getIngredientCategoryDao() throws SQLException {
        if (ingredientCategoryDao == null) {
            ingredientCategoryDao = getDao(IngredientCategory.class);
        }
        return ingredientCategoryDao;
    }

    /**
     * Returns the RuntimeExceptionDao (Database Access Object) version of a Dao for our IngredientCategory class. It will
     * create it or just give the cached value. RuntimeExceptionDao only through RuntimeExceptions.
     */
    public RuntimeExceptionDao<IngredientCategory, Integer> getIngredientCategoryRuntimeDao() {
        if (ingredientCategoryRuntimeDao == null) {
            ingredientCategoryRuntimeDao = getRuntimeExceptionDao(IngredientCategory.class);
        }
        return ingredientCategoryRuntimeDao;
    }



    /**
     * Returns the Database Access Object (DAO) for our CocktailIngredient class. It will create it or just give the cached
     * value.
     */
    public Dao<CocktailIngredient, Integer> getCocktailIngredientDao() throws SQLException {
        if (cocktailIngredientDao == null) {
            cocktailIngredientDao = getDao(CocktailIngredient.class);
        }
        return cocktailIngredientDao;
    }



    /**
     * Returns the RuntimeExceptionDao (Database Access Object) version of a Dao for our CocktailIngredient class. It will
     * create it or just give the cached value. RuntimeExceptionDao only through RuntimeExceptions.
     */
    public RuntimeExceptionDao<CocktailIngredient, Integer> getCocktailIngredientRuntimeDao() {
        if (cocktailIngredientRuntimeDao == null) {
            cocktailIngredientRuntimeDao = getRuntimeExceptionDao(CocktailIngredient.class);
        }
        return cocktailIngredientRuntimeDao;
    }




    /**
     * Returns the Database Access Object (DAO) for our Preference class. It will create it or just give the cached
     * value.
     */
    public Dao<Preference, Integer> getPreferenceDao() throws SQLException {
        if (preferenceDao == null) {
            preferenceDao = getDao(Preference.class);
        }
        return preferenceDao;
    }



    /**
     * Returns the RuntimeExceptionDao (Database Access Object) version of a Dao for our Preference class. It will
     * create it or just give the cached value. RuntimeExceptionDao only through RuntimeExceptions.
     */
    public RuntimeExceptionDao<Preference, Integer> getPreferenceRuntimeDao() {
        if (preferenceRuntimeDao == null) {
            preferenceRuntimeDao = getRuntimeExceptionDao(Preference.class);
        }
        return preferenceRuntimeDao;
    }



    /**
     * Returns the Database Access Object (DAO) for our Favorite class. It will create it or just give the cached
     * value.
     */
    public Dao<Favorite, Integer> getFavoriteDao() throws SQLException {
        if (favoriteDao == null) {
            favoriteDao = getDao(Favorite.class);
        }
        return favoriteDao;
    }



    /**
     * Returns the RuntimeExceptionDao (Database Access Object) version of a Dao for our Preference class. It will
     * create it or just give the cached value. RuntimeExceptionDao only through RuntimeExceptions.
     */
    public RuntimeExceptionDao<Favorite, Integer> getFavoriteRuntimeDao() {
        if (favoriteRuntimeDao == null) {
            favoriteRuntimeDao = getRuntimeExceptionDao(Favorite.class);
        }
        return favoriteRuntimeDao;
    }



    /**
     * Closes the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
        ingredientDao = null;
        ingredientRuntimeDao = null;
        cocktailDao = null;
        cocktailRuntimeDao = null;
        ingredientCategoryDao = null;
        ingredientCategoryRuntimeDao = null;
        cocktailIngredientDao = null;
        cocktailIngredientRuntimeDao = null;
        preferenceDao = null;
        preferenceRuntimeDao = null;
    }

    /**
     * Finds every ingredient for a cocktail
     *
     * @param cocktail Cocktail the cocktail
     * @return ingredients List<Ingredients> that contains every ingredient for
     * the given cocktail
     */
    public List<Ingredient> getCocktailIngredients(Cocktail cocktail) {
        cocktailIngredientRuntimeDao = getCocktailIngredientRuntimeDao();
        ingredientRuntimeDao = getIngredientRuntimeDao();
        ingredientCategoryRuntimeDao = getIngredientCategoryRuntimeDao();

        List<Ingredient> ingredients = null;

        try {
            QueryBuilder<Ingredient, Integer> ingredientQb = ingredientRuntimeDao.queryBuilder();
            QueryBuilder<CocktailIngredient, Integer> cocktailIngredientQb = cocktailIngredientRuntimeDao.queryBuilder();
            QueryBuilder<IngredientCategory, Integer> ingredientCategoryQb = ingredientCategoryRuntimeDao.queryBuilder();

            //Where cocktail.id = :id_cocktail
            cocktailIngredientQb.where().eq(CocktailIngredient.COCKTAIL_ID_FIELD_NAME, cocktail.getId());

            //SELECT id, name, category_id FROM ingredient
            ingredients = ingredientQb.
                    selectColumns(Ingredient.ID_FIELD_NAME, Ingredient.NAME_FIELD_NAME, Ingredient.INGREDIENT_CATEGORY_ID_FIELD).
                    //INNER JOIN cocktail_ingredient ON ingredient.id = cocktail_ingredient.ingredient_id
                    join(Ingredient.ID_FIELD_NAME, CocktailIngredient.INGREDIENT_ID_FIELD_NAME, cocktailIngredientQb).
                    //INNER JOIN ingredient_category ON ingredient_category.id = ingredient.category.id
                    join(Ingredient.INGREDIENT_CATEGORY_ID_FIELD, IngredientCategory.ID_FIELD_NAME, ingredientCategoryQb).query()
                    ;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch(NullPointerException e) {
            e.printStackTrace();
        }
        return ingredients;
    }


    /**
     * Inserts a cocktail into the database and associate it with ingredients
     * in database
     * @param cocktail Cocktail the cocktail to create
     * @param ingredients List<Ingredient> The ingredients to associate with the cocktail
     * @return int the number of inserted lines
     */
    public int createCocktail(Cocktail cocktail, List<Ingredient> ingredients) {
        int insertedLines = 0;
        cocktailIngredientRuntimeDao = getCocktailIngredientRuntimeDao();
        cocktailRuntimeDao = getCocktailRuntimeDao();

        //Insert cocktail into database
        cocktailRuntimeDao.create(cocktail);
        //For each ingredient (they have to already exist in database) link it to the cocktail
        for(Ingredient ingredient:ingredients) {
            CocktailIngredient cocktailIngredient = new CocktailIngredient(cocktail, ingredient);
            insertedLines += cocktailIngredientRuntimeDao.create(cocktailIngredient);
        }
        return insertedLines;
    }


    /**
     * Create user's preferences about Ingredients
     * @param preferences ArrayList<Preference> the preferences to insert
     * @return int the number of inserted preferences
     */
    public int createPreferences(ArrayList<Preference> preferences) {
        preferenceRuntimeDao = getPreferenceRuntimeDao();
        return preferenceRuntimeDao.create(preferences);
    }


    /**
     * Create user's favorites about Cocktail
     * @param favorites ArrayList<Favorite> the favorites to insert
     * @return int the number of inserted favorites
     */
    public int createFavorites(ArrayList<Favorite> favorites) {
        favoriteRuntimeDao = getFavoriteRuntimeDao();
        return favoriteRuntimeDao.create(favorites);
    }


    /**
     * Create ingredients in the database
     * @param ingredients ArrayList<Ingredient> the ingredients to insert
     * @return int the number of ingredients inserted
     */
    public int createIngredients(ArrayList<Ingredient> ingredients)
    {
        ingredientRuntimeDao = getIngredientRuntimeDao();
        return ingredientRuntimeDao.create(ingredients);
    }

    /**
     * Find an Ingredient according to its name
     * @param name String
     * @return Ingredient
     */
    public Ingredient findIngredient(String name)
    {
        //Find ingedient the user does not like
        ingredientRuntimeDao = getIngredientRuntimeDao();
        List<Ingredient> ingredients = ingredientRuntimeDao.queryForEq(Ingredient.NAME_FIELD_NAME, name);
        Ingredient unliked;
        if (ingredients != null && ingredients.size() > 0) {
            //Find
            return ingredients.get(0);
        }
        return null;
    }

    /**
     * Find all preferences
     * @return Collection
     */
    public Collection<Preference> findAllPreferences()
    {
        preferenceRuntimeDao = getPreferenceRuntimeDao();
        return preferenceRuntimeDao.queryForAll();
    }



    /**
     * Creates all entities
     */
    public void createEntities() {
        RuntimeExceptionDao<Ingredient, Integer> ingredientDao = getIngredientRuntimeDao();
        RuntimeExceptionDao<IngredientCategory, Integer> ingredientCategoryDao = getIngredientCategoryRuntimeDao();
        RuntimeExceptionDao<Cocktail, Integer> cocktailDao = getCocktailRuntimeDao();
        RuntimeExceptionDao<CocktailIngredient, Integer> cocktailIngredientDao = getCocktailIngredientRuntimeDao();
        RuntimeExceptionDao<Preference, Integer> preferenceDao = getPreferenceRuntimeDao();

        //------------------Categories-----------------------
        IngredientCategory alcohol = new IngredientCategory();
        alcohol.setName(context.getString(R.string.alcohol));
        ingredientCategoryDao.create(alcohol);

        IngredientCategory drinkWithoutAlcohol = new IngredientCategory();
        drinkWithoutAlcohol.setName(context.getString(R.string.drinkWithoutAlcohol));
        ingredientCategoryDao.create(drinkWithoutAlcohol);

        IngredientCategory fruit = new IngredientCategory();
        fruit.setName(context.getString(R.string.fruit));
        ingredientCategoryDao.create(fruit);

        IngredientCategory plant = new IngredientCategory();
        plant.setName(context.getString(R.string.plant));
        ingredientCategoryDao.create(plant);

        IngredientCategory syrup = new IngredientCategory();
        syrup.setName(context.getString(R.string.syrup));
        ingredientCategoryDao.create(syrup);


        //------------------Ingredients----------------------

        //------Alcohol------
        Ingredient rum = new Ingredient();
        rum.setName(context.getString(R.string.rum));
        rum.setCategory(alcohol);
        ingredientDao.create(rum);

        Ingredient vodka = new Ingredient();
        vodka.setName(context.getString(R.string.vodka));
        vodka.setCategory(alcohol);
        ingredientDao.create(vodka);

        Ingredient absinthe = new Ingredient();
        absinthe.setName(context.getString(R.string.absinthe));
        absinthe.setCategory(alcohol);
        ingredientDao.create(absinthe);

        Ingredient ale = new Ingredient();
        ale.setName(context.getString(R.string.ale));
        ale.setCategory(alcohol);
        ingredientDao.create(ale);

        Ingredient beer = new Ingredient();
        beer.setName(context.getString(R.string.beer));
        beer.setCategory(alcohol);
        ingredientDao.create(beer);

        Ingredient cognac = new Ingredient();
        cognac.setName(context.getString(R.string.cognac));
        cognac.setCategory(alcohol);
        ingredientDao.create(cognac);

        Ingredient gin = new Ingredient();
        gin.setName(context.getString(R.string.gin));
        gin.setCategory(alcohol);
        ingredientDao.create(gin);

        Ingredient tequila = new Ingredient();
        tequila.setName(context.getString(R.string.tequila));
        tequila.setCategory(alcohol);
        ingredientDao.create(tequila);

        Ingredient whisky = new Ingredient();
        whisky.setName(context.getString(R.string.whisky));
        whisky.setCategory(alcohol);
        ingredientDao.create(whisky);

        Ingredient bourbon = new Ingredient();
        bourbon.setName(context.getString(R.string.bourbon));
        bourbon.setCategory(alcohol);
        ingredientDao.create(bourbon);

        Ingredient pastis = new Ingredient();
        pastis.setName(context.getString(R.string.pastis));
        pastis.setCategory(alcohol);
        ingredientDao.create(pastis);

        Ingredient redWine = new Ingredient();
        redWine.setName(context.getString(R.string.redWine));
        redWine.setCategory(alcohol);
        ingredientDao.create(redWine);

        Ingredient whiteWine = new Ingredient();
        whiteWine.setName(context.getString(R.string.whiteWhine));
        whiteWine.setCategory(alcohol);
        ingredientDao.create(whiteWine);

        Ingredient roseWine = new Ingredient();
        roseWine.setName(context.getString(R.string.roseWine));
        roseWine.setCategory(alcohol);
        ingredientDao.create(roseWine);

        Ingredient portWine = new Ingredient();
        portWine.setName(context.getString(R.string.portWine));
        portWine.setCategory(alcohol);
        ingredientDao.create(portWine);

        Ingredient sparkling = new Ingredient();
        sparkling.setName(context.getString(R.string.sparkling));
        sparkling.setCategory(alcohol);
        ingredientDao.create(sparkling);

        Ingredient triple_sec = new Ingredient();
        triple_sec.setName(context.getString(R.string.triple_sec));
        triple_sec.setCategory(alcohol);
        ingredientDao.create(triple_sec);


        //-------Plant--------
        Ingredient mint = new Ingredient();
        mint.setName(context.getString(R.string.mint));
        mint.setCategory(plant);
        ingredientDao.create(mint);


        //-------Fruit-------
        Ingredient greenLemon = new Ingredient();
        greenLemon.setName(context.getString(R.string.greenLemon));
        greenLemon.setCategory(fruit);
        ingredientDao.create(greenLemon);

        //-------Drink without alcohol------
        Ingredient greenLemonJuice = new Ingredient();
        greenLemonJuice.setName(context.getString(R.string.greenLemonJuice));
        greenLemonJuice.setCategory(drinkWithoutAlcohol);
        ingredientDao.create(greenLemonJuice);

        Ingredient water = new Ingredient();
        water.setName(context.getString(R.string.water));
        water.setCategory(drinkWithoutAlcohol);
        ingredientDao.create(water);

        Ingredient sparklingWater = new Ingredient();
        sparklingWater.setName(context.getString(R.string.sparklingWater));
        sparklingWater.setCategory(drinkWithoutAlcohol);
        ingredientDao.create(sparklingWater);

        //------Syrup------
        Ingredient caneSugar = new Ingredient();
        caneSugar.setName(context.getString(R.string.caneSugar));
        caneSugar.setCategory(syrup);
        ingredientDao.create(caneSugar);



        //------------------------Cocktail-----------------------------------
        Cocktail mojito = new Cocktail();
        mojito.setName(context.getString(R.string.mojito));
        mojito.setDescription("Le mojito1 est un cocktail à base de rhum, de citron vert et de feuilles de menthe fraîche, né à Cuba dans les années 1910 et inspiré du mint julep.");
        mojito.setImage("mojito");
        mojito.setFavorite(false);
        cocktailDao.create(mojito);

        cocktailIngredientDao.create(new CocktailIngredient(mojito, rum));
        cocktailIngredientDao.create(new CocktailIngredient(mojito, greenLemonJuice));
        cocktailIngredientDao.create(new CocktailIngredient(mojito, mint));
        cocktailIngredientDao.create(new CocktailIngredient(mojito, sparklingWater));
        cocktailIngredientDao.create(new CocktailIngredient(mojito, caneSugar));



        Cocktail margarita = new Cocktail();
        margarita.setName("Margarita");
        margarita.setDescription("The margarita is a cocktail consisting of tequila, triple sec and lime or lemon juice, often served with salt on the rim of the glass. [note 1] The drink is served shaken with ice (on the rocks), blended with ice (frozen margarita), or without ice (straight up).");
        margarita.setFavorite(false);
        cocktailDao.create(margarita);

        cocktailIngredientDao.create(new CocktailIngredient(margarita, tequila));
        cocktailIngredientDao.create(new CocktailIngredient(margarita, greenLemonJuice));
        cocktailIngredientDao.create(new CocktailIngredient(margarita, triple_sec));

        //------------------------Preference-----------------------------------
        Preference preference1 = new Preference();
        preference1.setIngredient(mint);
        preference1.setLike(false);
        preferenceDao.create(preference1);
    }
}
