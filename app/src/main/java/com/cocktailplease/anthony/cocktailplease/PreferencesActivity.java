package com.cocktailplease.anthony.cocktailplease;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.cocktailplease.anthony.cocktailplease.db.entities.Cocktail;
import com.cocktailplease.anthony.cocktailplease.db.entities.CocktailIngredient;
import com.cocktailplease.anthony.cocktailplease.db.entities.Ingredient;
import com.cocktailplease.anthony.cocktailplease.db.entities.Preference;
import com.cocktailplease.anthony.cocktailplease.db.helpers.DatabaseHelper;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

public class PreferencesActivity extends OrmLiteBaseActivity<DatabaseHelper> implements MultiSelectionSpinner.OnMultipleItemsSelectedListener {

    private DrawerLayout drawerLayout;

    private Button reset_btn, submit_btn;

    private MultiSelectionSpinner multiSelectionSpinner;

    private Dao<Ingredient, Integer> ingredientDao;

    private Dao<Preference, Integer> preferencesDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_preferences);

        ////////////////////////////////////////////////////////// Menu
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ImageButton btnMenu = (ImageButton) findViewById(R.id.menu);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Overrides
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);

            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawerLayout.closeDrawers();
                        switch (menuItem.getItemId()) {

                            case R.id.nav_preferences:
                                return true;
                            case R.id.nav_suggestions:
                                onSuggestionsClick(findViewById(R.id.main_content));
                                return true;
                            case R.id.nav_createMine:
                                onCreateMyCocktailClick(findViewById(R.id.main_content));
                                return true;
                            default:
                                Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                                return true;

                        }
                    }
                });
        ////////////////////////////////////////////////////////// Menu

        try {

            /* Get all the ingredients from database */
            ingredientDao = getHelper().getIngredientDao();
            List<Ingredient> ingredientList = ingredientDao.queryForAll();

            /* Put the name of each ingredient object into a String Array[] */
            String String_Array[]=new String[ingredientList.size()];
            for (int i=0;i<String_Array.length;i++) String_Array[i]=ingredientList.get(i).getName().toString();

            /* Set the string array to the multiselect spinner */
            multiSelectionSpinner = (MultiSelectionSpinner) findViewById(R.id.mySpinner);
            multiSelectionSpinner.setItems(String_Array);
            multiSelectionSpinner.setListener(this);

            preferencesDao = getHelper().getPreferenceDao();
            List<Ingredient> ingredientsList = ingredientDao.queryBuilder().where().in(Ingredient.ID_FIELD_NAME, preferencesDao.queryBuilder().selectColumns(Preference.INGREDIENT_ID_FIELD_NAME).where().eq(Preference.LIKE_FIELD_NAME, false).query()).query();

            for (Ingredient ingredient : ingredientsList) {
                multiSelectionSpinner.setSelection(ingredient.getId().intValue());
            }

        } catch (java.sql.SQLException e) {

            e.printStackTrace();

        }

        reset_btn = (Button) findViewById(R.id.reset_btn);
        submit_btn = (Button) findViewById(R.id.submit_btn);

        android.view.View.OnClickListener onClick_method = new android.view.View.OnClickListener() {

            /**
             * On reset or submit button click
             * create cocktail or reset fields
             *
             * @param v View
             */
            @Override
            public void onClick(View v) {

                if(v == submit_btn)
                {

                    List<String> selectedIngredients = multiSelectionSpinner.getSelectedStrings();

                    try {
                        Dao<Preference, Integer> preferencesDao = getHelper().getPreferenceDao();
                        //Delete all preferences to add only the selected
                        preferencesDao.delete(getHelper().findAllPreferences());

                        ArrayList<Preference> preferences = new ArrayList<>();
                        //For each selected ingredient, find it and add into preferences
                        for (String ingredientName : selectedIngredients) {
                            Ingredient ingredient = getHelper().findIngredient(ingredientName);
                            if (ingredient != null) {
                                Preference p = new Preference();
                                p.setLike(false);
                                p.setIngredient(ingredient);
                                preferences.add(p);
                            }
                        }
                        //Insert preferences into database
                        getHelper().createPreferences(preferences);

                        Snackbar.make(v, "Your preferences have been added", Snackbar.LENGTH_LONG)
                                .setAction("Hide", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                }).show();
                    }
                    catch (java.sql.SQLException e) {

                    }
                }
                else if(v == reset_btn)
                {

                    multiSelectionSpinner.setSelection(0);

                }
            }
        };

        reset_btn.setOnClickListener(onClick_method);
        submit_btn.setOnClickListener(onClick_method);

    }

    /**
     * On Suggestions Button click, create new Intent
     * and display user's suggestions
     *
     * @param v View
     */
    public void onSuggestionsClick(View v) {
        Intent intent = new Intent(PreferencesActivity.this, SuggestionsActivity.class);
        startActivity(intent);
    }

    /**
     * On CreateMyCocktail Button, create new Intent
     * and allow user to create its own cocktail
     *
     * @param v View
     */
    public void onCreateMyCocktailClick(View v) {
        Intent intent = new Intent(PreferencesActivity.this, CreatemineActivity.class);
        startActivity(intent);
    }

    /**
     * On Home Button click, create new Intent
     * and go back to home
     *
     * @param v View
     */
    public void onButtonHomeClick(View v) {
        Intent intent = new Intent(PreferencesActivity.this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * Return the selected ingredients indices
     *
     * @param indices List<Integer>
     */
    @Override
    public void selectedIndices(List<Integer> indices) {

    }

    /**
     * Return the selected ingredients name
     *
     * @param strings List<String>
     */
    @Override
    public void selectedStrings(List<String> strings) {
        Toast.makeText(this, strings.size() + " ingredients added", Toast.LENGTH_LONG).show();
    }

}
