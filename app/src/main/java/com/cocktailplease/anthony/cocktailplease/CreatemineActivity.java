package com.cocktailplease.anthony.cocktailplease;

import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.cocktailplease.anthony.cocktailplease.db.entities.Cocktail;
import com.cocktailplease.anthony.cocktailplease.db.entities.CocktailIngredient;
import com.cocktailplease.anthony.cocktailplease.db.entities.Ingredient;
import com.cocktailplease.anthony.cocktailplease.db.helpers.DatabaseHelper;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class CreatemineActivity extends OrmLiteBaseActivity<DatabaseHelper> implements MultiSelectionSpinner.OnMultipleItemsSelectedListener {

    int RESULT_LOAD_IMAGE = 1;
    String picturePath = null;
    private DrawerLayout drawerLayout;

    private EditText cocktail_name, description;
    private Button reset_btn, submit_btn;

    private MultiSelectionSpinner multiSelectionSpinner;

    private Dao<Ingredient, Integer> ingredientDao;
    private Dao<CocktailIngredient, Integer> cocktailIngredientDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_createmine);

        ////////////////////////////////////////////////////////// Menu
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ImageButton btnMenu = (ImageButton) findViewById(R.id.menu);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Overrides
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);

            }
        });



        Button addImageButton = (Button) findViewById(R.id.add_image);
        addImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(
                        Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawerLayout.closeDrawers();
                        switch (menuItem.getItemId()) {

                            case R.id.nav_preferences:
                                onPreferencesClick(findViewById(R.id.main_content));
                                return true;
                            case R.id.nav_suggestions:
                                onSuggestionsClick(findViewById(R.id.main_content));
                                return true;
                            case R.id.nav_createMine:
                                return true;
                            default:
                                Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                                return true;

                        }
                    }
                });
        ////////////////////////////////////////////////////////// Menu

        /*Cocktail cocktail = getHelper().getCocktailRuntimeDao().queryForAll();
        List<Ingredient> ings = getHelper().getCocktailIngredients(cocktail);*/

        try {

            /* Get all the ingredients from database */
            ingredientDao = getHelper().getIngredientDao();
            List<Ingredient> ingredientList = ingredientDao.queryForAll();

            /* Put the name of each ingredient object into a String Array[] */
            String String_Array[]=new String[ingredientList.size()];
            for (int i=0;i<String_Array.length;i++) String_Array[i]=ingredientList.get(i).getName().toString();

            /* Set the string array to the multiselect spinner */
            multiSelectionSpinner = (MultiSelectionSpinner) findViewById(R.id.mySpinner);
            multiSelectionSpinner.setItems(String_Array);
            multiSelectionSpinner.setListener(this);

        } catch (java.sql.SQLException e) {

            e.printStackTrace();

        }

        cocktail_name = (EditText) findViewById(R.id.name_field);
        description = (EditText) findViewById(R.id.description_field);
        reset_btn = (Button) findViewById(R.id.reset_btn);
        submit_btn = (Button) findViewById(R.id.submit_btn);

        android.view.View.OnClickListener onClick_method = new android.view.View.OnClickListener() {

            /**
             * On reset or submit button click
             * create cocktail or reset fields
             *
             * @param v View
             */
            @Override
            public void onClick(View v) {

                if(v == submit_btn)
                {
                    // We check if all fields are filled
                    if(cocktail_name.getText().toString().trim().length() > 0 && description.getText().toString().trim().length() > 0)
                    {
                        Cocktail cocktail = new Cocktail();

                        // Then, set all the values from user input to the cocktail object
                        cocktail.setName(cocktail_name.getText().toString());
                        cocktail.setDescription(description.getText().toString());


                        //Save the picture
                        try {
                            // create a File object for the parent directory
                            File directory = getFilesDir();
                            // have the object build the directory structure, if needed.
                            directory.mkdirs();

                            //The selected file in gallery
                            File fileToCopy = new File(picturePath);

                            // create a File object for the output file in our directory
                            String[] nameParts = fileToCopy.getName().split("\\.");
                            String extension = nameParts[nameParts.length-1];
                            File outputFile = new File(directory, cocktail.getName() + '.' + extension);
                            copy(fileToCopy, outputFile);
                            //Save the image path
                            cocktail.setImage(outputFile.getAbsolutePath());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        List<String> selectedIngredients = multiSelectionSpinner.getSelectedStrings();


                        // Create a cocktail and show a snackbar
                        try {
                            Dao<Cocktail, Integer> cocktailDao = getHelper().getCocktailDao();
                            cocktailDao.create(cocktail);

                            cocktailIngredientDao = getHelper().getCocktailIngredientDao();
                            QueryBuilder<Ingredient, Integer> queryBuilder = ingredientDao.queryBuilder();

                            for (int i=0;i<selectedIngredients.size();i++) {
                                Ingredient ingredient;
                                ingredient = queryBuilder.where().eq(Ingredient.NAME_FIELD_NAME, selectedIngredients.get(i)).queryForFirst();
                                cocktailIngredientDao.create(new CocktailIngredient(cocktail, ingredient));
                            }

                            cocktail_name.setText("");
                            description.setText("");
                            multiSelectionSpinner.setSelection(0);

                            Snackbar.make(v, "Your cocktail have been created", Snackbar.LENGTH_LONG)
                                    .setAction("Hide", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                        }
                                    }).show();
                            picturePath = null;
                        } catch (SQLException e) {
                            e.printStackTrace();
                        } catch (java.sql.SQLException e) {
                            e.printStackTrace();
                        }
                    }
                    // Show a snackbar if some fields are empty
                    else
                    {
                        Snackbar.make(v, "Some fields are empty !", Snackbar.LENGTH_LONG)
                                .setAction("Hide", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                }).show();
                    }
                }
                else if(v == reset_btn)
                {
                    cocktail_name.setText("");
                    description.setText("");
                    multiSelectionSpinner.setSelection(0);
                }
            }
        };

        reset_btn.setOnClickListener(onClick_method);
        submit_btn.setOnClickListener(onClick_method);

    }


    /**
     * Copy a file from a source to a destination
     * @param src File
     * @param dst File
     * @throws IOException
     */
    public void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }








    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            // String picturePath contains the path of selected Image

        }
    }








    /**
     * On Preferences Button click, create new Intent
     * and display user's preferences
     *
     * @param v View
     */
    public void onPreferencesClick(View v) {

        Intent intent = new Intent(CreatemineActivity.this, PreferencesActivity.class);
        startActivity(intent);
    }


    /**
     * On Suggestions Button click, create new Intent
     * and display user's suggestions
     *
     * @param v View
     */
    public void onSuggestionsClick(View v) {
        Intent intent = new Intent(CreatemineActivity.this, SuggestionsActivity.class);
        startActivity(intent);
    }

    /**
     * On Home Button click, create new Intent
     * and go back to home
     *
     * @param v View
     */
    public void onButtonHomeClick(View v) {
        Intent intent = new Intent(CreatemineActivity.this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * Return the selected ingredients indices
     *
     * @param indices List<Integer>
     */
    @Override
    public void selectedIndices(List<Integer> indices) {

    }

    /**
     * Return the selected ingredients name
     *
     * @param strings List<String>
     */
    @Override
    public void selectedStrings(List<String> strings) {
        Toast.makeText(this, strings.size() + " ingredients added", Toast.LENGTH_LONG).show();
    }
}
