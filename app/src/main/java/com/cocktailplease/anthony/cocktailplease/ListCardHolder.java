package com.cocktailplease.anthony.cocktailplease;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cocktailplease.anthony.cocktailplease.db.entities.Cocktail;

/**
 * Created by Gilles on 17/05/2016.
 */
public class ListCardHolder extends RecyclerView.ViewHolder{

    private TextView textViewView;
    private TextView cocktail_id;
    private ImageView imageView;


    //itemView is the view of 1 cell
    public ListCardHolder(View itemView) {
        super(itemView);

        //here we get the different views with findView

        cocktail_id = (TextView) itemView.findViewById(R.id.cocktail_id);
        textViewView = (TextView) itemView.findViewById(R.id.text_card);
        imageView = (ImageView) itemView.findViewById(R.id.image_card);
    }

    //We add a function to fill the cell with the cocktail object datas
    public void bind(Cocktail cocktail){
        cocktail_id.setText(cocktail.getId().toString());
        textViewView.setText(cocktail.getName());
        imageView.setBackground(Drawable.createFromPath(cocktail.getImage()));
    }
}
