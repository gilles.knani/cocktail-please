package com.cocktailplease.anthony.cocktailplease;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cocktailplease.anthony.cocktailplease.db.entities.Cocktail;

import java.util.List;

/**
 * Created by Gilles on 17/05/2016.
 */
public class ListCardAdapter extends RecyclerView.Adapter<ListCardHolder> {

    List<Cocktail> list;

    //ajouter un constructeur prenant en entrée une liste
    public ListCardAdapter(List<Cocktail> list) {
        this.list = list;
    }

    //cette fonction permet de créer les viewHolder
    //et par la même indiquer la vue à inflater (à partir des layout xml)
    @Override
    public ListCardHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_card_small,viewGroup,false);
        return new ListCardHolder(view);
    }

    //c'est ici que nous allons remplir notre cellule avec le texte/image de chaque MyObjects
    @Override
    public void onBindViewHolder(ListCardHolder myViewHolder, int position) {
        Cocktail cocktail = list.get(position);
        myViewHolder.bind(cocktail);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}