package com.cocktailplease.anthony.cocktailplease;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.cocktailplease.anthony.cocktailplease.db.entities.Cocktail;
import com.cocktailplease.anthony.cocktailplease.db.entities.CocktailIngredient;
import com.cocktailplease.anthony.cocktailplease.db.entities.Ingredient;
import com.cocktailplease.anthony.cocktailplease.db.entities.Preference;
import com.cocktailplease.anthony.cocktailplease.db.helpers.DatabaseHelper;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class SuggestionsActivity extends OrmLiteBaseActivity<DatabaseHelper> {

    private DrawerLayout drawerLayout;

    private RecyclerView recyclerView;

    private List<Cocktail> cocktailList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_suggestions);

        ////////////////////////////////////////////////////////// Menu
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ImageButton btnMenu = (ImageButton) findViewById(R.id.menu);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Overrides
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);

            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawerLayout.closeDrawers();
                        switch (menuItem.getItemId()) {

                            case R.id.nav_preferences:
                                onPreferencesClick(findViewById(R.id.main_content));
                                return true;
                            case R.id.nav_suggestions:
                                return true;
                            case R.id.nav_createMine:
                                onCreateMyCocktailClick(findViewById(R.id.main_content));
                                return true;
                            default:
                                Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                                return true;

                        }
                    }
                });
        ////////////////////////////////////////////////////////// Menu

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // Display cards as grid (2 cards/row)
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        // Get the cocktails and display them
        try {

            Dao<Cocktail, Integer> cocktailDao = getHelper().getCocktailDao();
            Dao<CocktailIngredient, Integer> cocktailIngredientDao = getHelper().getCocktailIngredientDao();
            Dao<Ingredient, Integer> ingredientDao = getHelper().getIngredientDao();
            Dao<Preference, Integer> preferencesDao = getHelper().getPreferenceDao();

            List<Cocktail> cocktailList = cocktailDao.queryBuilder().where().in(Cocktail.ID_FIELD_NAME, cocktailIngredientDao.queryBuilder().selectColumns(CocktailIngredient.COCKTAIL_ID_FIELD_NAME).where().in(CocktailIngredient.INGREDIENT_ID_FIELD_NAME, preferencesDao.queryBuilder().where().eq(Preference.LIKE_FIELD_NAME, true).query()).query()).query();
            Log.d("nb_cocktail", "" + cocktailList.size());

            int viewHeight = 1000 * Math.round(cocktailList.size()/2);
            recyclerView.getLayoutParams().height = viewHeight;

            // Fill the recyclerview with data
            recyclerView.setAdapter(new ListCardAdapter(cocktailList));

        } catch (SQLException e) {
            e.printStackTrace();
        }


        ////////////////////// SearchView

        final android.widget.SearchView searchView = (android.widget.SearchView) findViewById(R.id.action_search);
        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Get the cocktails and display them
                try {

                    Dao<Cocktail, Integer> cocktailDao = getHelper().getCocktailDao();
                    if (query.isEmpty()) {
                        List<Cocktail> cocktailList = cocktailDao.queryBuilder().where().eq(Cocktail.FAVORITE_FIELD_NAME, true).query();

                        int viewHeight = (int) (1000 * Math.round((double) cocktailList.size() / (double) 2));
                        recyclerView.getLayoutParams().height = viewHeight;


                        // Fill the recyclerview with data
                        recyclerView.setAdapter(new ListCardAdapter(cocktailList));
                    } else {
                        List<Cocktail> cocktailList = cocktailDao.queryBuilder().where().like(Cocktail.NAME_FIELD_NAME, "%" + query + "%").and().eq(Cocktail.FAVORITE_FIELD_NAME, true).query();

                        int viewHeight = (int) (1000 * Math.round((double) cocktailList.size() / (double) 2));
                        recyclerView.getLayoutParams().height = viewHeight;
                        Log.d("nb_lignes", "" + (int) Math.ceil(cocktailList.size() / 2));


                        // Fill the recyclerview with data
                        recyclerView.setAdapter(new ListCardAdapter(cocktailList));
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d("Requete :", newText);
                // Get the cocktails and display them
                try {

                    Dao<Cocktail, Integer> cocktailDao = getHelper().getCocktailDao();
                    if (newText.isEmpty()) {
                        List<Cocktail> cocktailList = cocktailDao.queryBuilder().where().eq(Cocktail.FAVORITE_FIELD_NAME, true).query();

                        int viewHeight = (int) (1000 * Math.round((double) cocktailList.size() / (double) 2));
                        recyclerView.getLayoutParams().height = viewHeight;


                        // Fill the recyclerview with data
                        recyclerView.setAdapter(new ListCardAdapter(cocktailList));
                    } else {
                        List<Cocktail> cocktailList = cocktailDao.queryBuilder().where().like(Cocktail.NAME_FIELD_NAME, "%" + newText + "%").and().eq(Cocktail.FAVORITE_FIELD_NAME, true).query();

                        int viewHeight = (int) (1000 * Math.round((double) cocktailList.size() / (double) 2));
                        recyclerView.getLayoutParams().height = viewHeight;
                        Log.d("nb_lignes", "" + (int) Math.ceil(cocktailList.size() / 2));


                        // Fill the recyclerview with data
                        recyclerView.setAdapter(new ListCardAdapter(cocktailList));
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return false;
            }
        });


    }

    /**
     * On Preferences Button click, create new Intent
     * and display user's preferences
     *
     * @param v View
     */
    public void onPreferencesClick(View v) {

        /*Cocktail cocktail = getHelper().getCocktailRuntimeDao().queryForAll().get(0);
        List<Ingredient> ings = getHelper().getCocktailIngredients(cocktail);

        int x = (ings == null) ? -1 : ings.size();

        //Long x = (cocktail == null) ? 0 : cocktail.getId();
        Toast.makeText(this, "Preferences " + x, Toast.LENGTH_LONG).show();*/

        Intent intent = new Intent(SuggestionsActivity.this, PreferencesActivity.class);
        startActivity(intent);
    }


    /**
     * On CreateMyCocktail Button, create new Intent
     * and allow user to create its own cocktail
     *
     * @param v View
     */
    public void onCreateMyCocktailClick(View v) {
        Intent intent = new Intent(SuggestionsActivity.this, CreatemineActivity.class);
        startActivity(intent);
    }

    /**
     * On Home Button click, create new Intent
     * and go back to home
     *
     * @param v View
     */
    public void onButtonHomeClick(View v) {
        Intent intent = new Intent(SuggestionsActivity.this, MainActivity.class);
        startActivity(intent);
    }

}
