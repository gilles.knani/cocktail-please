package com.cocktailplease.anthony.cocktailplease;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cocktailplease.anthony.cocktailplease.db.entities.Cocktail;
import com.cocktailplease.anthony.cocktailplease.db.entities.Ingredient;
import com.cocktailplease.anthony.cocktailplease.db.helpers.DatabaseHelper;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import org.w3c.dom.Text;

import java.util.List;

public class CocktailDetailsActivity extends OrmLiteBaseActivity<DatabaseHelper> {

    private DrawerLayout drawerLayout;
    private FloatingActionButton btnBackHome;
    private FloatingActionButton addToFavoritesBtn;
    private FloatingActionButton removeFromFavoritesBtn;

    private TextView cocktail_description;
    private TextView cocktail_title;
    private TextView cocktail_ingredients;

    private ImageView imageView;

    private Cocktail cocktail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cocktail_details);

        ////////////////////////////////////////////////////////// Titre de l'application
        TextView cocktail_title = (TextView) findViewById(R.id.title);
        setFont(cocktail_title, "Satisfy-Regular.ttf");
        ////////////////////////////////////////////////////////// Titre de l'application

        ////////////////////////////////////////////////////////// Menu
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ImageButton btnMenu = (ImageButton) findViewById(R.id.menu);
        Button btnFavorites = (Button) findViewById(R.id.favorites);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Overrides
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);

            }
        });

        btnFavorites.setOnClickListener(new View.OnClickListener() {
            @Overrides
            public void onClick(View v) {
                onFavoritesClick(findViewById(R.id.main_content));
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawerLayout.closeDrawers();
                        switch (menuItem.getItemId()) {

                            case R.id.nav_preferences:
                                onPreferencesClick(findViewById(R.id.main_content));
                                return true;
                            case R.id.nav_suggestions:
                                onSuggestionsClick(findViewById(R.id.main_content));
                                return true;
                            case R.id.nav_createMine:
                                onCreateMyCocktailClick(findViewById(R.id.main_content));
                                return true;
                            default:
                                Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                                return true;

                        }
                    }
                });
        ////////////////////////////////////////////////////////// Menu

        btnBackHome = (FloatingActionButton) findViewById(R.id.backHome);
        addToFavoritesBtn = (FloatingActionButton) findViewById(R.id.AddToFavorites);
        removeFromFavoritesBtn = (FloatingActionButton) findViewById(R.id.RemoveFromFavorites);


        ///////////////////////////////////////////////////////// Get the cocktail datas from database and display them

        // Get the cocktail id passed in the intent
        Intent intent = getIntent();
        Integer cocktail_id = intent.getIntExtra("id_cocktail", -1);
        Log.d("Cocktail id", "" + cocktail_id);

        cocktail = getHelper().getCocktailRuntimeDao().queryForAll().get(cocktail_id);


        if (cocktail.getFavorite()) {

            removeFromFavoritesBtn.setVisibility(View.VISIBLE);
            addToFavoritesBtn.setVisibility(View.GONE);

        } else {

            addToFavoritesBtn.setVisibility(View.VISIBLE);
            removeFromFavoritesBtn.setVisibility(View.GONE);

        }

        imageView = (ImageView) findViewById(R.id.backgroundImageView);
        imageView.setBackground(Drawable.createFromPath(cocktail.getImage()));

        // Display the cocktail name
        cocktail_title.setText(cocktail.getName());

        // Display the cocktail description
        cocktail_description = (TextView) findViewById(R.id.cocktail_description);
        cocktail_description.setText(cocktail.getDescription());

        // Get the ingredients list associated to the cocktail
        cocktail_ingredients = (TextView) findViewById(R.id.ingredients);
        List<Ingredient> ings = getHelper().getCocktailIngredients(cocktail);

        // Check if the ingredient list is empty and diplsay a default message
        if (ings.size() == 0) {
            cocktail_ingredients.setText("No ingredients were set to this cocktail");
        }
        // Else display all the ingredients in a coma separated string
        else {

            String ingredients = "";
            for (int i=0;i<ings.size();i++) {

                ingredients += (i == 0) ? ings.get(i).getName() : ", "+ings.get(i).getName();
            }

            cocktail_ingredients.setText(ingredients);
        }

        ///////////////////////////////////////////////////////// Get the cocktail datas from database
    }

    public void setFont(TextView textView, String fontName) {
        if(fontName != null){
            try {
                Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/" + fontName);
                textView.setTypeface(typeface);
            } catch (Exception e) {
                Log.e("FONT", fontName + " not found", e);
            }
        }
    }

    /**
     * On Preferences Button click, create new Intent
     * and display user's preferences
     *
     * @param v View
     */
    public void onPreferencesClick(View v) {

        /*Cocktail cocktail = getHelper().getCocktailRuntimeDao().queryForAll().get(0);
        List<Ingredient> ings = getHelper().getCocktailIngredients(cocktail);

        int x = (ings == null) ? -1 : ings.size();

        //Long x = (cocktail == null) ? 0 : cocktail.getId();
        Toast.makeText(this, "Preferences " + x, Toast.LENGTH_LONG).show();*/

        Intent intent = new Intent(CocktailDetailsActivity.this, PreferencesActivity.class);
        //intent.putExtra(VARIABLE_A_ENVOYER, ICI_ON_RECUPERE_VALEUR_A_ENVOYER);
        startActivity(intent);
    }

    /**
     * On Suggestions Button click, create new Intent
     * and display user's suggestions
     *
     * @param v View
     */
    public void onSuggestionsClick(View v) {
        Intent intent = new Intent(CocktailDetailsActivity.this, SuggestionsActivity.class);
        startActivity(intent);
    }

    /**
     * On Favorites Button click, create new Intent
     * and display user's favorites
     *
     * @param v View
     */
    public void onFavoritesClick(View v) {
        Intent intent = new Intent(CocktailDetailsActivity.this, FavoritesActivity.class);
        startActivity(intent);
    }

    /**
     * On Add Button click, create new Intent
     * and add cocktail to favorites
     *
     * @param v View
     */
    public void onButtonAddToFavoritesClick(View v) {

        cocktail.setFavorite(true);
        getHelper().getCocktailRuntimeDao().createOrUpdate(cocktail);

        v.setVisibility(View.GONE);
        removeFromFavoritesBtn.setVisibility(View.VISIBLE);

        Snackbar.make(v, "Cocktail added to favorites", Snackbar.LENGTH_LONG)
                .setAction("Hide", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
    }

    /**
     * On Add Button click, create new Intent
     * and add cocktail to favorites
     *
     * @param v View
     */
    public void onButtonRemoveFromFavoritesClick(View v) {

        cocktail.setFavorite(false);
        getHelper().getCocktailRuntimeDao().createOrUpdate(cocktail);

        v.setVisibility(View.GONE);
        addToFavoritesBtn.setVisibility(View.VISIBLE);

        Snackbar.make(v, "Cocktail removed from favorites", Snackbar.LENGTH_LONG)
                .setAction("Hide", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
    }

    /**
     * On CreateMyCocktail Button, create new Intent
     * and allow user to create its own cocktail
     *
     * @param v View
     */
    public void onCreateMyCocktailClick(View v) {
        Intent intent = new Intent(CocktailDetailsActivity.this, CreatemineActivity.class);
        startActivity(intent);
    }

    /**
     * On Home Button click, create new Intent
     * and go back to home
     *
     * @param v View
     */
    public void onButtonHomeClick(View v) {
        Intent intent = new Intent(CocktailDetailsActivity.this, MainActivity.class);
        startActivity(intent);
    }

}
