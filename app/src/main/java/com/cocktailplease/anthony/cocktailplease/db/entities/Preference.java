package com.cocktailplease.anthony.cocktailplease.db.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Represents a user preference about ingredients
 * Created by anthony on 28/02/16.
 */
@DatabaseTable(tableName = "preference")
public class Preference {

    public final static String ID_FIELD_NAME = "id";
    public final static String INGREDIENT_ID_FIELD_NAME = "ingredient_id";
    public final static String LIKE_FIELD_NAME = "like";


    @DatabaseField(generatedId = true, columnName = ID_FIELD_NAME)
    private Long id;

    @DatabaseField(foreign = true, columnName = INGREDIENT_ID_FIELD_NAME, uniqueCombo = true)
    private Ingredient ingredient;

    //True if the user like this ingredient, false else
    @DatabaseField(columnName = LIKE_FIELD_NAME, defaultValue = "true", uniqueCombo = true)
    private boolean like;




    public Preference() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }
}
