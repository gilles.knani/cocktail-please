package com.cocktailplease.anthony.cocktailplease;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.cocktailplease.anthony.cocktailplease.db.entities.Cocktail;
import com.cocktailplease.anthony.cocktailplease.db.entities.Ingredient;
import com.cocktailplease.anthony.cocktailplease.db.helpers.DatabaseHelper;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends OrmLiteBaseActivity<DatabaseHelper> {

    private DrawerLayout drawerLayout;

    private RecyclerView recyclerView;

    private List<Cocktail> cocktailList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ////////////////////////////////////////////////////////// Titre de l'application
        TextView textView1 = (TextView) findViewById(R.id.app_title);
        setFont(textView1, "Satisfy-Regular.ttf");
        ////////////////////////////////////////////////////////// Titre de l'application

        ////////////////////////////////////////////////////////// Menu
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ImageButton btnMenu = (ImageButton) findViewById(R.id.menu);
        Button btnFavorites = (Button) findViewById(R.id.favorites);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Overrides
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);

            }
        });

        btnFavorites.setOnClickListener(new View.OnClickListener() {
            @Overrides
            public void onClick(View v) {
                onFavoritesClick(findViewById(R.id.main_content));
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawerLayout.closeDrawers();
                        switch (menuItem.getItemId()) {

                            case R.id.nav_preferences:
                                onPreferencesClick(findViewById(R.id.main_content));
                                return true;
                            case R.id.nav_suggestions:
                                onSuggestionsClick(findViewById(R.id.main_content));
                                return true;
                            case R.id.nav_createMine:
                                onCreateMyCocktailClick(findViewById(R.id.main_content));
                                return true;
                            default:
                                Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                                return true;

                        }
                    }
                });
        ////////////////////////////////////////////////////////// Menu

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // Display cards as grid (2 cards/row)
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        // Get the cocktails and display them
        try {

            Dao<Cocktail, Integer> cocktailDao = getHelper().getCocktailDao();
            List<Cocktail> cocktailList = cocktailDao.queryForAll();

            /*if (cocktailList == null || cocktailList.size() == 0) {

            }*/

            int viewHeight = 1000 * Math.round(cocktailList.size()/2);
            recyclerView.getLayoutParams().height = viewHeight;
            Log.d("nb_lignes",""+(int)Math.ceil(cocktailList.size()/2));

            // Fill the recyclerview with data
            recyclerView.setAdapter(new ListCardAdapter(cocktailList));

        } catch (SQLException e) {
            e.printStackTrace();
        }


        ////////////////////// SearchView

        final android.widget.SearchView searchView = (android.widget.SearchView) findViewById(R.id.action_search);
        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Get the cocktails and display them
                try {

                    Dao<Cocktail, Integer> cocktailDao = getHelper().getCocktailDao();
                    if (query.isEmpty()) {
                        List<Cocktail> cocktailList = cocktailDao.queryForAll();

                        int viewHeight = (int)(1000 * Math.round((double)cocktailList.size()/(double)2));
                        recyclerView.getLayoutParams().height = viewHeight;


                        // Fill the recyclerview with data
                        recyclerView.setAdapter(new ListCardAdapter(cocktailList));
                    } else {
                        List<Cocktail> cocktailList = cocktailDao.queryBuilder().where().like(Cocktail.NAME_FIELD_NAME, "%" + query + "%").query();

                        int viewHeight = (int)(1000 * Math.round((double)cocktailList.size()/(double)2));
                        recyclerView.getLayoutParams().height = viewHeight;
                        Log.d("nb_lignes",""+(int)Math.ceil(cocktailList.size()/2));


                        // Fill the recyclerview with data
                        recyclerView.setAdapter(new ListCardAdapter(cocktailList));
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d("Requete :", newText);
                // Get the cocktails and display them
                try {

                    Dao<Cocktail, Integer> cocktailDao = getHelper().getCocktailDao();
                    if (newText.isEmpty()) {
                        List<Cocktail> cocktailList = cocktailDao.queryForAll();

                        int viewHeight = (int)(1000 * Math.round((double)cocktailList.size()/(double)2));
                        recyclerView.getLayoutParams().height = viewHeight;


                        // Fill the recyclerview with data
                        recyclerView.setAdapter(new ListCardAdapter(cocktailList));
                    } else {
                        List<Cocktail> cocktailList = cocktailDao.queryBuilder().where().like(Cocktail.NAME_FIELD_NAME, "%" + newText + "%").query();

                        int viewHeight = (int)(1000 * Math.round((double)cocktailList.size()/(double)2));
                        recyclerView.getLayoutParams().height = viewHeight;
                        Log.d("nb_lignes",""+(int)Math.ceil(cocktailList.size()/2));


                        // Fill the recyclerview with data
                        recyclerView.setAdapter(new ListCardAdapter(cocktailList));
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return false;
            }
        });




    }

    public void setFont(TextView textView, String fontName) {
        if(fontName != null){
            try {
                Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/" + fontName);
                textView.setTypeface(typeface);
            } catch (Exception e) {
                Log.e("FONT", fontName + " not found", e);
            }
        }
    }

    /**
     * On Preferences Button click, create new Intent
     * and display user's preferences
     *
     * @param v View
     */
    public void onPreferencesClick(View v) {

        /*Cocktail cocktail = getHelper().getCocktailRuntimeDao().queryForAll().get(0);
        List<Ingredient> ings = getHelper().getCocktailIngredients(cocktail);

        int x = (ings == null) ? -1 : ings.size();

        //Long x = (cocktail == null) ? 0 : cocktail.getId();
        Toast.makeText(this, "Preferences " + x, Toast.LENGTH_LONG).show();*/

        Intent intent = new Intent(MainActivity.this, PreferencesActivity.class);
        startActivity(intent);
    }

    /**
     * On Suggestions Button click, create new Intent
     * and display user's suggestions
     *
     * @param v View
     */
    public void onSuggestionsClick(View v) {
        Intent intent = new Intent(MainActivity.this, SuggestionsActivity.class);
        startActivity(intent);
    }

    /**
     * On Favorites Button click, create new Intent
     * and display user's favorites
     *
     * @param v View
     */
    public void onFavoritesClick(View v) {
        Intent intent = new Intent(MainActivity.this, FavoritesActivity.class);
        startActivity(intent);
    }

    /**
     * On Add Button click, create new Intent
     * and go to Create My Cocktail view
     *
     * @param v View
     */
    public void onButtonAddClick(View v) {
        Intent intent = new Intent(MainActivity.this, CreatemineActivity.class);
        startActivity(intent);
    }

    /**
     * On CreateMyCocktail Button, create new Intent
     * and allow user to create its own cocktail
     *
     * @param v View
     */
    public void onCreateMyCocktailClick(View v) {
        Intent intent = new Intent(MainActivity.this, CreatemineActivity.class);
        startActivity(intent);
    }

    /**
     * On Cocktail click, create new Intent
     * and go to cocktail details view
     *
     * @param v View
     */
    public void onCocktailClick(View v) {

        TextView cocktail_id = (TextView) v.findViewById(R.id.cocktail_id);
        Integer id = Integer.parseInt(cocktail_id.getText().toString());

        Intent intent = new Intent(MainActivity.this, CocktailDetailsActivity.class);
        intent.putExtra("id_cocktail", id-1);
        startActivity(intent);
    }

}
