package com.cocktailplease.anthony.cocktailplease.db.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Represents user's favorite cocktails
 * Created by anthony on 28/02/16.
 */
@DatabaseTable(tableName = "favorite")
public class Favorite {

    public final static String ID_FIELD_NAME = "id";
    public final static String COCKTAIL_ID_FIELD_NAME = "cocktail_id";

    @DatabaseField(generatedId = true, columnName = ID_FIELD_NAME)
    private Long id;

    @DatabaseField(columnName = COCKTAIL_ID_FIELD_NAME, unique = true)
    private Cocktail cocktail;





    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cocktail getCocktail() {
        return cocktail;
    }

    public void setCocktail(Cocktail cocktail) {
        this.cocktail = cocktail;
    }
}
