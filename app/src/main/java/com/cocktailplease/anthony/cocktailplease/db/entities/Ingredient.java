package com.cocktailplease.anthony.cocktailplease.db.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Represents an Ingredient
 * Created by anthony on 24/02/16.
 */
@DatabaseTable(tableName = "ingredient")
public class Ingredient {

    public final static String ID_FIELD_NAME = "id";

    public final static String INGREDIENT_CATEGORY_ID_FIELD = "category_id";

    public final static String NAME_FIELD_NAME = "name";


    /**
     * Id the Ingredient's id in the database
     */
    @DatabaseField(generatedId = true, columnName = ID_FIELD_NAME)
    private Long id;

    /**
     * Name the Ingredient's name in the database
     */
    @DatabaseField(canBeNull = false, columnName = NAME_FIELD_NAME, unique = true)
    private String name;

    /**
     * IngredientCategory the ingredient's category
     * The category can be automatically inserted by ORMLite
     */
    @DatabaseField(foreign = true, foreignAutoCreate = true, columnName = INGREDIENT_CATEGORY_ID_FIELD,
            useGetSet = true, foreignColumnName = IngredientCategory.ID_FIELD_NAME)
    private IngredientCategory category;



    /**
     * ORMLite needs a no-argument constructor
     */
    public Ingredient() {
    }

    public Long getId() { return id; }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IngredientCategory getCategory() {
        return category;
    }

    public void setCategory(IngredientCategory category) {
        this.category = category;
    }

}
