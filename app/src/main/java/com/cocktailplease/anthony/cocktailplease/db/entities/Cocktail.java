package com.cocktailplease.anthony.cocktailplease.db.entities;

import android.support.annotation.NonNull;

import com.j256.ormlite.dao.BaseForeignCollection;
import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.CloseableWrappedIterable;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.EagerForeignCollection;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Represents a Cocktail
 * Created by anthony on 26/02/16.
 */
@DatabaseTable(tableName = "cocktail")
public class Cocktail {

    public final static String ID_FIELD_NAME = "id";

    public final static String NAME_FIELD_NAME = "name";

    public final static String DESCRIPTION_FIELD_NAME = "description";

    public final static String IMAGE_FIELD_NAME = "image";

    public final static String FAVORITE_FIELD_NAME = "favorite";


    /**
     * Id the Cocktail's id in the database
     */
    @DatabaseField(generatedId = true, columnName = ID_FIELD_NAME)
    private Integer id;

    /**
     * String the cocktail's name
     */
    @DatabaseField(canBeNull = false, columnName = NAME_FIELD_NAME)
    private String name;


    /**
     * String the cocktail's description
     */
    @DatabaseField(canBeNull = false, columnName = DESCRIPTION_FIELD_NAME)
    private String description;

    /**
     * String the cocktail's image
     */
    @DatabaseField(canBeNull = false, defaultValue = "mojito.png" , columnName = IMAGE_FIELD_NAME)
    private String image;

    /**
     * Boolean cocktail is favorite ?
     */
    @DatabaseField(canBeNull = true, defaultValue = "false", columnName = FAVORITE_FIELD_NAME)
    private Boolean favorite;





    public Cocktail() {

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }
}
