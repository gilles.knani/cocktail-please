package com.cocktailplease.anthony.cocktailplease.db.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Represents a link between Cocktail and Ingredient, because
 * ORMLite does not support ManyToMany relation
 * Created by anthony on 26/02/16.
 */
public class CocktailIngredient {

    public final static String COCKTAIL_ID_FIELD_NAME = "cocktail_id";

    public final static String INGREDIENT_ID_FIELD_NAME = "ingredient_id";


    public final static String ID_FIELD_NAME = "id";

    @DatabaseField(generatedId = true, columnName = ID_FIELD_NAME)
    Long id;

    /**
     * The cocktail
     */
    @DatabaseField(foreign = true, columnName = COCKTAIL_ID_FIELD_NAME, uniqueCombo = true)
    private Cocktail cocktail;

    /**
     * An ingredient in the cocktail
     */
    @DatabaseField(foreign = true, columnName = INGREDIENT_ID_FIELD_NAME, uniqueCombo = true)
    private Ingredient ingredient;


    public CocktailIngredient() {
    }


    public CocktailIngredient(Cocktail cocktail, Ingredient ingredient) {
        this.cocktail = cocktail;
        this.ingredient = ingredient;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cocktail getCocktail() {
        return cocktail;
    }

    public void setCocktail(Cocktail cocktail) {
        this.cocktail = cocktail;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }
}
