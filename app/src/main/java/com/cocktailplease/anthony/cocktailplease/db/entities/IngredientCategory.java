package com.cocktailplease.anthony.cocktailplease.db.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Represents an ingredient category
 * Created by anthony on 26/02/16.
 */
@DatabaseTable(tableName = "ingredient_category")
public class IngredientCategory {

    public final static String ID_FIELD_NAME = "id";

    public final static String NAME_FIELD_NAME = "name";


    /**
     * Id the IngredientCategory's id in the database
     */
    @DatabaseField(generatedId = true, columnName = ID_FIELD_NAME)
    private Long id;

    /**
     * Name the IngredientCategory's name in the database
     */
    @DatabaseField(canBeNull = false, columnName = NAME_FIELD_NAME)
    private String name;



    public IngredientCategory() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
